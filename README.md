Projects for Informatics 133: User Interaction Software at UC Irvine in Fall 2014.
Real-time data retrieval using AJAX calls to retrieve data formatted in XML, JSON, and JSON-P. 

Note: Projects may no longer work due to changes to APIs and XSS standards.